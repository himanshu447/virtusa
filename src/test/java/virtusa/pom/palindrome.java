package virtusa.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class palindrome {

	public WebDriver driver;

	By palindrome = By.xpath("//*[@id=\"originalWord\"]");
	By palindromesubmit = By.xpath("//*[@id=\"button1\"]");
	By Anagram = By.xpath("//*[@id=\"anagramWord\"]");
	By Anagramsubmit = By.xpath("//*[@id=\"button2\"]");
	
	public palindrome(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

		public WebElement getpalindrome() {
		return driver.findElement(palindrome);
	}
		public WebElement getpalindromesubmit() {
			return driver.findElement(palindromesubmit);
		}
	public WebElement getAnagram() {
		return driver.findElement(Anagram);
	}
	
	public WebElement getAnagramsubmit() {
		return driver.findElement(Anagramsubmit);
	}

	}

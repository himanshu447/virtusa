package virtusa.scripts;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import virtusa.pom.palindrome;

public class LoginForModules {

	public static WebDriver driver;

	@BeforeSuite()
	public void setup() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Exefiles/chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("https://xndev.com/palindrome");
		driver.manage().window().maximize();
		Thread.sleep(4000);
		palindrome pl= new palindrome(driver);
		pl.getpalindrome().sendKeys("palindrome");
		pl.getpalindromesubmit().click();
		pl.getAnagram().sendKeys("anagram");
		pl.getAnagramsubmit().click();



	}

	@Test()
	public void modulesHistory() throws InterruptedException, RowsExceededException, WriteException, IOException {

	}

	@AfterSuite()

	public void tearDown() {
		// driver.quit();

	}
}
